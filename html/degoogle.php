<!DOCTYPE HTML>
<html>

<head>
    <title>penloy.xyz | Degoogle</title>

    <meta charset="UTF-8">
    <meta name="author" content="Penloy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.min.css">
</head>

<body>
    <div class="flex-container">
        <?php include '../sidebar.php';?>
        <div id="content">
            <main>
            <h1>Degoogle</h1>
            <p>
                In the modern capitalist landscape, there are few peaceful things you can do to detach yourself from
                capitalism in general. Alienation from the product of your labour is inescapable; it is experienced by
                almost every worker under capitalism. The majority of workers are forced to work month-month,
                paycheck-paycheck. Most working class academics have to take out ridiculous amounts of student debt. You
                are required to take on car payments if you live far away from your workplace. Most workers work jobs
                that they hate or don't like, and the only thing that keeps them sane is their coworkers.
            </p>
            <p>
                Few things can be done to detach from capitalism.
            </p>
            <p>
                Google is a mega-corp that tracks all of your location data, all of your Youtube watch-history, all of
                your search queries, everything you say to your Google Home device; Google tracks almost every aspect of
                your life, without you even knowing. Google profits greatly from their data-centers full of information
                about you by selling your data to advertisers. Google probably owns your phones operating system and a
                lot of the apps in there. Google is an awful, privacy unconscious capitalist entity, and you should
                advocate for its destruction on every level.
            </p>
            <p>
                If it's difficult to <em>not</em> have a Google account, that's probably a sign that you should run, and
                run as fast as you can.
            </p>
            <p>
                When we talk about de-googling, we not only talk about it in terms of you personally deleting all of
                your Google services (although that would be a great start), we talk about it in terms of the greater
                material conditions we're living in; in others words: we're talking about telling small and large
                companies to stop using Google in their own services. A de-google movement has recently been created
                called 'ReverseEagle', who's goal is to reach out to open-source projects and ask them politely, via
                emails and git pull-requests, to stop using Google. I will place links to where you can find
                ReverseEagle on <a href="https://dev.lemmy.ml/">Lemmy</a> and <a href="https://matrix.org/">Matrix</a>,
                two great open-source alternatives to Reddit and Discord respectively.
            </p>
            <h2>So how can you personally do your part?</h2>
            <p>
                A good start would be to begin using <a href="https://invidio.us/">Invidious</a> as your YouTube video
                viewing platform; Invidious has every YouTube video available over there, you just take a youtube link,
                delete <b>youtube.com</b> and replace it with <b>invidio.us</b>. You can use a Firefox extension called
                <a href="https://addons.mozilla.org/en-US/firefox/addon/redirector/">Redirector</a> to setup a redirect
                so that any youtube video will redirect to its Invidious counterpart. I will place an image below this
                paragraph to show you how to set this up. In addition to using Invidious, you can use <a
                    href="https://searx.info/">SearX</a> to replace Google! SearX takes a multitude of search engines
                and compiles them into one list, it even can show Google search results. The advantage of using SearX is
                that it tries to remove any ads and trackers from the results. In fact, this is also what Invidious
                does. Another advantage of SearX is that you can host your own instance on a VPS or home server.
            </p>
            <img src="/images/redirect.png">
            <p>
                If you've done all that, ensure to replace your email with something like <a
                    href="https://www.tutanota.com/">Tutanota</a> or <a href="https://protonmail.com/">Protonmail</a>.
                Personally I use Protonmail, and I like it even more than I did Gmail.
            </p>
            <p>
                An important step would be to join ReverseEagle and join the movement to remove Google from open source.
                Help us push for change, and make open-source a better, more private, safer place for all of us. I will
                post links to all of the relevant resources down below.
            </p>
            <ul>
                <li>Join the ReverseEagle lemmy over at <a
                        href="https://dev.lemmy.ml/c/reverseeagle">https://dev.lemmy.ml/c/reverseeagle</a>.</li>
                <li>Join our Matrix instance. If you don't yet have a Matrix account, head over to <a
                        href="https://riot.im/">Riot.im</a> to create an account and to download a sexy, E2EE matrix
                    client. Once you've done that, join our <a
                        href="https://matrix.to/#/!TDiDZaaFzkUSHRWcxQ:chat.endl.site?via=chat.endl.site&via=matrix.org">ReverseEagle</a>
                    Matrix instance!</li>
                <li>Check out the <a href="https://pages.codeberg.org/ReverseEagle/">Codeberg pages site for
                        ReverseEagle</a>.</li>
            </ul>
            </main>
        </div>
    </div>
</body>

</html>