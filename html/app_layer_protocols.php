<!DOCTYPE HTML>
<html>

<head>
    <title>penloy.xyz | App Layer Protocols</title>

    <meta charset="UTF-8">
    <meta name="author" content="Penloy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.min.css">
</head>

<body>
    <div class="flex-container">
        <?php include '../sidebar.php';?>
        <div id="content">
            <main>
                <h1>Application Layer Protocols</h1>
                <p>
                    To truly understand how to make an application layer protocol, it is important to understand
                    existing
                    examples of application layer protocols.
                </p>
                <p>
                    There are lots of different types of existing application layer protocols that are well established.
                    These categories are:
                </p>
                <ul>
                    <li>Management Protocols (BOOTP, DNS, SNMP, NTP)</li>
                    <li>Remote Communication Protocols (Telnet, SSH)</li>
                    <li>File Transfer Protocols (FTP, TFTP)</li>
                    <li>Mail Application Protocols (SMTP, POP3, IMAP4)</li>
                    <li>Browser Protocols (HTTP, HTTPS, TLS/SSL)</li>
                    <li>Voice over IP (VoIP) Protocols (SIP, RTP)</li>
                </ul>
                <h2>Management Protcols</h2>
                <h3>BOOTP-Bootstrap Protocol</h3>
                <p>
                    BootP will automate the IP Address configuration process. Today, this is replaced by Dynamic Host
                    Configuration Protocol (DHCP)
                </p>
                <h3>DNS - Domain Name Services</h3>
                <p>
                    DNS converts domain names to IP addresses. For example, it will convert example.com into 192.0.43.10
                    so
                    you don't have to remember 192.0.43.10.
                </p>
                <h3>NTP - Network Time Protocol</h3>
                <p>
                    NTP will automatically synchronize clocks over a network.
                </p>
                <h3>SNMP(v3) - Simple Network Management Protocol</h3>
                <p>
                    SNMP(v3) will gather statistics from network devices. Useful for gathering information from network
                    devices like "How many bytes have gone into this network device?". Version 3 is the only encrypted
                    version of this protocol, so it is recommended to use version 3 always.
                </p>
                <h2>Remote Communication Protocols</h2>
                <h3>Telnet - Telecommunication Network</h3>
                <p>
                    Telnet can be used to remote connect to another machine over a network (Like SSH but unencrypted).
                    Since
                    it's unencrypted, it's not recommended that you use this protocol to connect to any machine with
                    private
                    data.
                </p>
                <h3>SSH - Secure Shell</h3>
                <p>
                    SSH can be used to remote into another machine, except unlike Telnet, SSH is of course secure and
                    encrypted (end to end), so you do not need to worry about the security of the connection.
                </p>
                <h2>File Transfer Protocols</h2>
                <h3>FTP - File Transfer Protocol</h3>
                <p>
                    Allows the transfer of files between two machines. This is similar to a remote communication
                    protocol,
                    however it is only used for the transfer of files (technically SSH can also be used to transfer
                    files
                    through the scp command, but FTP was built for file transfer and it's probably better for doing such
                    a
                    thing in most cases). FTP offers a full suite of functionality for transferring and modifying files
                    on a
                    remote machine.
                </p>
                <h3>TFTP - Trivial File Transfer Protocol</h3>
                <p>
                    This is a much simpler way to transfer files. It can only read and write files, so not but better
                    than
                    just using SSH. In fact, unless your use case requires it, you really shouldn't use TFTP ever
                    because it
                    requires no authentication to transfer files if set up on a machine. Anybody can connect and read or
                    write files this way without authentication.
                </p>
                <h2>Mail Application Protocols</h2>
                <h3>SMTP - Simple Mail Transfer Protocol</h3>
                <p>
                    SMTP is often used for sending mail between mail servers. We need another protocol for receiving
                    mail.
                </p>
                <h3>POP3 - Post Office Protocol Version 3</h3>
                <p>
                    POP3 is used to receive mail, and it is designed for intermittent connectivity.
                </p>
                <h3>IMAP4 - Internet Message Access Protocol v4</h3>
                <p>
                    IMAP4 is another mail protocol similar to POP3, however IMAP4 is a lot more functional. It keeps
                    states
                    like Read, Replied, Deleted, etc…
                </p>
                <h2>Browser Application Protocols</h2>
                <h3>HTTP - Hypertext Transfer Protocol</h3>
                <p>
                    HTTP is the most common browser protocols there are, and it's even used in non browser applications.
                </p>
                <h3>HTTPS - Hypertext Transfer Protocol Secure</h3>
                <p>
                    Just HTTP with an extra layer of encryption through TLS/SSL. HTTPS ensure your data does not get
                    intercepted by a man-in-the-middle attack. If someone tries to intercept your data, it would be
                    almost
                    impossible to decrypt.
                </p>
                <h3>TLS/SSL - Transport Layer Security and Secure Sockets Layer</h3>
                <p>
                    TLS is the updated version of SSL. These are encryption layers to encrypt data in a browser.
                </p>
                <h2>Voice over IP (VoIP) Protocols</h2>
                <h3>SIP - Session Initiation Protocol</h3>
                <p>
                    Sets up and tears down media calls.
                </p>
                <h3>RTP - Real-Time Transport Protocol</h3>
                <p>
                    RTP is the main protocol used in VoIP along with SIP. This is the protocol that is used to actually
                    transfer media, like your voice, over the network. SIP is only used to set up the call, make sure
                    it's
                    still running, and tear down the call.
                </p>

                <h2>Sources</h2>
                <ul>
                    <li>https://www.youtube.com/embed/FjFkCEuCsL0</li>
                </ul>
            </main>
        </div>
    </div>
</body>

</html>