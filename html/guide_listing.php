<!DOCTYPE HTML>
<html>

<head>
    <title>penloys website</title>

    <meta charset="UTF-8">
    <meta name="author" content="Penloy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.min.css">
</head>

<body>
    <div class="flex-container">
        <?php include '../sidebar.php';?>
        <div id="content">
            <main>
                <h1>My Guides</h1>
                <ul>
                    <li><a href="#">First Guide (doesn't exist yet)</a></li>
                </ul>
            </main>
        </div>
    </div>
</body>

</html>