<!DOCTYPE HTML>
<html>

<head>
    <title>penloy.xyz | Taylist</title>

    <meta charset="UTF-8">
    <meta name="author" content="Penloy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.min.css">
</head>

<body>
    <div class="flex-container">
        <?php include '../sidebar.php';?>
        <div id="content">
            <main>
                <h1>Taylor Swift Song Ranking</h1>
                <p>The goal of this project is to systematically rank and review every Taylor Swift song and album.</p>
                <h2>Speak Now Ranking</h1>
                    <ol>
                        <li>Better than revenge - 98/100</li>
                        <li>Story of Us - 92/100</li>
                        <li>Enchanted - 91/100</li>
                        <li>Speak Now - 90/100</li>
                        <li>Haunted - 90/100</li>
                        <li>Never Grow Up - 90/100</li>
                        <li>Mean - 81/100</li>
                        <li>Long Live - 80/100</li>
                        <li>Dear John - 77/100</li>
                        <li>Mine - 75/100</li>
                        <li>Innocent - 70/100</li>
                        <li>Back to december - 69/100</li>
                        <li>Sparks Fly - 65/100</li>
                        <li>Last Kiss - 64/100</li>
                    </ol>
                    <h2>Red Ranking</h2>
                    <ol>
                        <li>All Too Well - 99/100</li>
                        <li>Holy Ground - 97</li>
                        <li>We Are Never Getting Back Together - 95/100</li>
                        <li>Red - 95/100</li>
                        <li>I Knew You Were Trouble - 95/100</li>
                        <li>22 - 95/100</li>
                        <li>Come Back... Be Here - 94/100</li>
                        <li>Stay Stay Stay - 93/100</li>
                        <li>The Moment I Knew - 90/100</li>
                        <li>I Almost Do - 90/100</li>
                        <li>Girl At Home - 88/100</li>
                        <li>Starlight - 88/100</li>
                        <li>The Last Time - 87/100</li>
                        <li>Everything Has Changed - 85/100</li>
                        <li>Sad Beautiful Tragic - 81/100</li>
                        <li>Everything Has Changed - 80/100</li>
                        <li>The Lucky One - 78/100</li>
                        <li>Treacherous - 73/100</li>
                        <li>Stace of Grace - 69/100</li>
                    </ol>
                    <h2>Lover Ranking</h2>
                    <ol>
                        <li>Paper Rings</li>
                        <li>The Man</li>
                        <li>Miss Americana and the Heartbreak Prince</li>
                        <li>Cornelia Street</li>
                        <li>Lover</li>
                        <li>Death By A Thousand Cuts</li>
                        <li>Soon You'll Get Better</li>
                        <li>Cruel Summer</li>
                        <li>I Forgot That You Existed</li>
                        <li>Afterglow</li>
                        <li>Daylight</li>
                        <li>I Think He Knows</li>
                        <li>The Archer</li>
                        <li>False God</li>
                        <li>You Need To Calm Down</li>
                        <li>It's Nice To Have A Friend</li>
                        <li>London Boy</li>
                        <li>Me!</li>
                    </ol>
            </main>
        </div>
    </div>
</body>

</html>