<!DOCTYPE HTML>
<html>

<head>
    <title>penloy.xyz | Contact Me</title>

    <meta charset="UTF-8">
    <meta name="author" content="Penloy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.min.css">
</head>

<body>
    <div class="flex-container">
        <?php include '../sidebar.php';?>
        <div id="content">
            <main>
                <h1>Contact Me</h1>

                <p>My contact email is contact@penloy.com (Yes, it's really .com, not .xyz)</p>
            </main>
        </div>
    </div>
</body>

</html>