<!DOCTYPE HTML>
<html>

<head>
    <title>penloy.xyz | Videos</title>

    <meta charset="UTF-8">
    <meta name="author" content="Penloy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.min.css">
</head>

<body>
    <div class="flex-container">
        <?php include '../sidebar.php';?>
        <div id="content">
            <main>
                <h1>My YouTube videos. These link directly to YouTube.</h1>
                <ul>
                    <li><a href="https://www.youtube.com/watch?v=qCZm8OR-R1Y">Hello World C++</a></li>
                </ul>
            </main>
        </div>
    </div>
</body>

</html>