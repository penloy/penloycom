<!DOCTYPE HTML>
<html>

<head>
    <title>penloy.xyz | Regex State Machines</title>

    <meta charset="UTF-8">
    <meta name="author" content="Penloy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.min.css">
</head>

<body>
    <div class="flex-container">
        <?php include '../sidebar.php';?>
        <div id="content">
            <main>
            <div id="header"></div>
        <h1>Why regular expressions should be used to represent finite state machines rather than state diagrams.</h1>
        <p>
        Finite state machines have inputs, outputs, and of course, a finite number of states.The states depend on the inputs, and the outputs depend on the states. State machines can be represented in many different ways, including state diagrams, regular expressions, Chomksy hierarchy, state tables, etc…
        </p>
        <p>
        A regular expression (regex) specifies a set of strings required for a particular purpose.[1].
        </p>
        <p>
        We can take a look at a light switch as an example of a finite state machine. The light switch has a finite number of states, “Light on” and “Light off”. As a regular expression, the light switch would be represented as: (ab)*.
        </p>
        <p>
        Whereas a state diagram for the same state machine would look like this:
        </p>
        <img src="/images/fsm.png">
        <p>
        As shown, (ab)* is a much simpler and shorter way of representing a finite state machine than drawing a diagram of one. This is even further emphasised when creating much more complex finite state machines than just a light switch. In addition, it would also be a lot easier to turn a regular expression into something a compiler can understand than it would be to turn a state diagram into something a compiler could understand.
        </p>
        <p>
        [1]https://en.wikipedia.org/wiki/Regular_expression
        </p>
        <div id="footer"></div>
            </main>
        </div>
    </div>
</body>

</html>

