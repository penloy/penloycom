<!DOCTYPE HTML>
<html>

<head>
    <title>penloy.xyz | BLM</title>

    <meta charset="UTF-8">
    <meta name="author" content="Penloy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.min.css">
</head>

<body>
    <div class="flex-container">
        <?php include '../sidebar.php';?>
        <div id="content">
            <main>
                <h1>Black Lives Matter</h1>
                <p>
                    In light of the recent black lives matter protests, we should show support for black lives that have
                    been taken by the police in America, and the movement for justice. Police need to be held
                    accountable
                    for their murders, and we need to rethink the system of racial oppression and profit that the
                    current
                    police system is built on in America.
                </p>
                <p>
                    A repository and api is being built to record and collect the police-driven murders of black people
                    during the current protests. This repository has been mirrored across websites by supporters of BLM
                    incase the original repository hosted on GitHub is taken down by Microsoft.
                </p>
                <ul>
                    <li><a href="https://github.com/2020PB/police-brutality">Original repository.</a></li>
                    <li><a href="https://gitlab.com/Penloy/police-brutality">Mirror on GitLab</a></li>
                    <li><a href="https://codeberg.org/TheMainOne/police-brutality">Mirror on Codeberg</a></li>
                </ul>
                <p>
                    Also consider donating to BLM organisations. <a
                        href="https://docs.google.com/document/d/1yLWGTQIe3967hdc9RSxBq5s6KKZHe-3_mWp5oemd7OA/preview?pru=AAABcpUiX3k*Y6Q4I6UBtkH3lLz9GVLg0A">Reclaim
                        The Block</a> is a community-compiled list of organisations to donate to in support of black
                    lives
                    matter.
                </p>
            </main>
        </div>
    </div>
</body>

</html>