<!DOCTYPE HTML>
<html>

<head>
    <title>penloy.xyz | Make A Website</title>

    <meta charset="UTF-8">
    <meta name="author" content="Penloy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.min.css">
</head>

<body>
    <div class="flex-container">
        <?php include '../sidebar.php';?>
        <div id="content">
            <main>
                <h1>Make a Website</h1>
                <h2>But why make a website?</h2>
                <p>
                    As a general philosophy, I feel that if you can detach yourself from the modern capitalist paradigm
                    that is Microsoft, Google, Amazon, Apple, and Facebook, you should. Detaching yourself from these
                    capitalist units not only frees yourself from needlessly participating in capitalism, to the
                    detriment of those companies, but it allows you to learn new technologies and expand your skillset.
                    Not only do you expand your own skillset, but you benefit the collective as well; people who want to
                    communicate with you or know what you're doing have to detach themselves from those platforms for at
                    least a small amount of time.
                </p>
                <p>
                    Instead of limiting yourself to Facebook/Twitter/online blog generators layout, you get to make your
                    own layout: the only limit when making your own website is your knowledge. Why limit yourself to
                    some other websites options? There really isn't a reason to except convenience, and in addition
                    there are lots of cons to doing it that way:
                </p>
                <ol>
                    <li>You limit yourself to the customisation options of that platform.</li>
                    <li>You are willingly handing over your data to that platform.</li>
                    <li>You are relying on their protocols to be secure enough to keep your information secure.</li>
                    <li>You are not learning anything new like you would if you made your own website.</li>
                </ol>
                <h2>But how do I make a website?</h2>
                <p>
                    You first need to learn HTML, which is quite simple. It's very intuitive. You have a range of
                    headings (large to small), and paragraphs. You have the title which is what appears in the tab, and
                    links. These are the basic building blocks of a website, and if you press F12 you can inspect the
                    HTML of this website to see how it's built. There are many tutorials on how to use HTML and I'll
                    link some at the bottom of this page. CSS is used to style your page, customise it's
                    colors/widths/etc... Really you don't need much CSS, you can also view the styles of this page in
                    F12. You can use javascript or php to make your website dynamic, but you don't have to.
                </p>
                <p>
                    You have to buy a VPS (they're usually around 5 dollars per month), and you need to set up nginx or
                    apache2 web server. You also need to buy a domain name at around 10 dollars per year and learn how
                    to link the DNS. To achieve HTTPS status (more secure http), you have to install a program called
                    letsencrypt on your VPS and run it. It's pretty simple to do. I will include links to tutorials for
                    everything mentioned here at the bottom of this page.
                </p>
                <ul>
                    <li><a href="https://www.learn-html.org/">Here</a>, <a
                            href="https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web">Here</a>,
                        and <a href="https://developer.mozilla.org/en-US/docs/Learn/HTML">Here</a> are some resources to
                        help you learn HTML.</li>
                    <li><a href="https://www.codecademy.com/learn/learn-css">Here</a>, <a
                            href="https://css-tricks.com/">Here</a>, and <a
                            href="https://developer.mozilla.org/en-US/docs/Learn/CSS">Here</a> are some resources to
                        help you learn CSS</li>
                    <li>You can buy a VPS from <a href="https://www.linode.com/">Linode</a> or <a
                            href="https://www.digitalocean.com/">DigitalOcean</a>. There are other VPS providers but
                        these are really cheap and well known and popular (not sponsored)</li>
                    <li><a href="https://www.namecheap.com/">Namecheap</a> are a pretty popular domain name provider,
                        I've not tried many but people seem to like this one.</li>
                    <li><a href="https://certbot.eff.org/lets-encrypt/ubuntufocal-apache">Certbot</a> will allow you to
                        secure your website with letsencrypt https!</li>
                    <li><a href="https://my.bluehost.com/hosting/help/what-are-dns-records">Learn DNS record
                            meanings</a> in order to be fully familiar with the registration process of a website.</li>
                </ul>
                <p>
                    So what are you waiting for? Go ahead and make that website!
                </p>
            </main>
        </div>
    </div>
</body>

</html>