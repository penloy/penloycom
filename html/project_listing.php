<!DOCTYPE HTML>
<html>

<head>
    <title>penloy.xyz | Projects</title>

    <meta charset="UTF-8">
    <meta name="author" content="Penloy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.min.css">
</head>

<body>
    <div class="flex-container">
        <?php include '../sidebar.php';?>
        <div id="content">
            <main>
                <h1>Projects I'm currently working on</h1>
                <ul>
                    <li><a href="/index.php">My Website</a></li>
                    <li><a href="/html/taylist.php">Taylor Swift Song Ranking</a></li>
                </ul>
            </main>
        </div>
    </div>
</body>

</html>