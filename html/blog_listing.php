<!DOCTYPE HTML>
<html>

<head>
    <title>penloy.xyz | Blogs</title>

    <meta charset="UTF-8">
    <meta name="author" content="Penloy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.min.css">
</head>

<body>
    <div class="flex-container">
        <?php include '../sidebar.php';?>
        <div id="content">
            <main>
                <h1>My Blogs</h1>
                <ul>
                    <!-- <li><a href="/html/a_look_at_c.php">A Look At C</a></li> -->
                    <li><a href="/html/degoogle.php">Degoogle</a></li>
                    <li><a href="/html/use_vim.php">Learn Vim</a></li>
                    <li><a href="/html/make_a_website.php">Make A Website</a></li>
                    <li><a href="/html/regex_state_machines.php">Why Regular Expressions Should Be Used To Represent
                            Finite
                            State Machines Rather Than State Diagrams.</a></li>
                    <li><a href="/html/app_layer_protocols.php">Application Layer Protocols</a></li>
                </ul>
            </main>
        </div>
    </div>
</body>

</html>