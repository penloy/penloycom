<!DOCTYPE HTML>
<html>

<head>
    <title>Penloy.xyz | A look at C</title>

    <meta charset="UTF-8">
    <meta name="author" content="Penloy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.min.css">
</head>

<body>
    <div class="flex-container">
        <?php include '../sidebar.php';?>
        <div id="content">
            <main>
                <h1>A look at C</h1>
                <p>As someone who studies game development, I often use C++, but rarely C. In university we are always given premade frameworks to work from, and only touch the surface of memory allocation and more low level operations. The closest we've gotten is graphics programming. Recently, I've wanted to explore the low level world of C. Therefore, I will be looking at various articles and noting my thoughts in this blog post.</p>

                <blockquote>
                C++ is a horrible language. It’s made more horrible by the fact that a lot of substandard programmers use it, to the point where it’s much much easier to generate total and utter crap with it. Quite frankly, even if the choice of C were to do *nothing* but keep the C++ programmers out, that in itself would be a huge reason to use C.

                <br><cite>Linus Torvalds</cite>
                </blockquote>

                <p>One intriguing aspect of C is its classless design. We aren't really taught to use functional programming design in game dev class; class-based design (Object-Oriented Programming) is enforced to the maximum. I want to get to know the reasons for using functional programming, as at the very least, it will give me perspective.</p>

            </main>
        </div>
    </div>
</body>

</html>