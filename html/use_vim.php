<!DOCTYPE HTML>
<html>

<head>
    <title>penloy.xyz | Use Vim</title>

    <meta charset="UTF-8">
    <meta name="author" content="Penloy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.min.css">
</head>

<body>
    <div class="flex-container">
        <?php include '../sidebar.php';?>
        <div id="content">
            <main>
            <h1>Use Vim.</h1>
            <h2>But why use Vim?</h2>
            <p>
                There are lots of reasons to use vim. Vim is an entirely different paradigm to the standard text editor,
                and has an extremely high learning curve, probably the biggest learning curve of any text editor or IDE.
                Despite the high learning curve, thousands of vim users still repeat: Use vim! Why would all of these
                people recommend such a difficult text editor, are they masochists?
            </p>
            <p>
                Truth is, vim is extremely rewarding to learn, and once you get past that mountain of a learning curve,
                you start to realise that when you use vim, you can do things <b>much</b> faster than before. You can
                use the mouse in vim, but the mouse is a crutch, not something you'll use permanently; the mouse is only
                there to help you transition. The fully-integrated, butter smooth keyboard commands are what make vim
                great to use. You may be resistant to the idea, you may think "This is so much work just to use a text
                editor", but please trust me when I say it is worth it to stick with it.
            </p>
            <p>
                The advantages to using vim might not be obvious, but I'll list a few:
            </p>
            <ol>
                <li>Vim is platform independent, it can be used anywhere. It can be used on Linux, MacOS, Windows,
                    iPadOS, iOS, Android. It can even be used in a VPS. Once vim is your preferred editor, you can open
                    up your fully configured, fully integrated vim in your VPS. This means you can edit in your
                    preferred environment and feel at home in your VPS!</li>
                <li>Vim is a fully configurable, modifiable text editor which can be transformed into an IDE-like
                    experience with full syntax highlighting, error checking, debugging experience with terminal
                    integration, file trees, you name it! Vim probably has it. I'll show an image of my fully configured
                    neovim below this list, and I'll link to my neovim config at the bottom of this page.</li>
                <li>You can experience a boost in efficiency like no other. There are hundreds, maybe thousands of time
                    savers that you can learn in vim. You can be really fast with just the basic few commands, but once
                    you start combining those keystrokes into more complex variations on commands, you get to be
                    extremely efficient and fast at editing text. It took weeks, even months for me to get where I am
                    today, but I can say safely that I am much faster at editing in vim than I ever was editing in a
                    regular IDE or text editor.</li>
                <li>Vim is <em>extremely</em> light weight and fast! It opens in less than half a second and runs in a
                    terminal, meaning you don't even have to leave your terminal to enter vim. It's right there are your
                    fingertips.</li>
            </ol>
            <img src="/images/neovim.png">
            <h2>But how do I learn vim?</h2>
            <p>
                First of all, I recommend just using neovim right out of the gate. Vim stands for vi improved, and vi is
                the predecessor to vim. Neovim is to vim like vim is to vi. Neovim has many improvements and QOL changes
                to vim that just make life better and easier.
            </p>
            <p>
                If you're now convinced you want to learn vim, I will provide a list of resources for you to be able to
                learn vim below.
            </p>
            <ul>
                <li><a href="https://www.youtube.com/channel/UC8ENHE5xdFSwx71u3fDH5Xw">ThePrimagen</a> is an entire
                    YouTube channel dedicated to helping people learn vim. Not only is it a great resource for learning
                    vim, but his videos are extremely entertaining and he also does livestreams. <a
                        href="https://www.youtube.com/watch?v=H3o4l4GVLW0">Vim as your editor</a> is a great series of
                    his to checkout for everyone, even if you're already experienced with vim. <a
                        href="https://www.youtube.com/watch?v=ICU9OEsNiRA">Vim life</a> is an older series but still
                    quite new and definitely worth checking out!</li>
                <li><a href="https://neovim.io/doc/">The neovim documentation</a> contains helpful docs if you just want
                    to get right into the meat of things.</li>
                <li><a href="https://gitlab.com/Penloy/nvim-config">My neovim config</a> of course is available. A
                    general rule of thumb though is that if you don't know what something in someone elses vim config
                    does, don't put it in your config. It might make things very confusing if something non-standard in
                    my config conflicts with information someone else tells you about how vim works.</li>
                <li><a href="https://www.youtube.com/watch?v=wlR5gYd6um0">Vim as a language </a>is part of a vim
                    conference and it talks about the fundamentals of vim as a language. It's super insightful into how
                    vim works and it's definitely worth checking out.</li>
            </ul>

            <p>
                So what are you waiting for? Go install vim!
            </p>
            </main>
        </div>
    </div>
</body>

</html>