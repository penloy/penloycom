<!DOCTYPE HTML>
<html>

<head>
    <title>penloy.xyz | Home</title>

    <meta charset="UTF-8">
    <meta name="author" content="Penloy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.min.css">
</head>

<body>
    <div class="flex-container">
        <?php include 'sidebar.php';?>
        <div id="content">
            <main>
                <h1>Welcome to penloy.xyz ^^</h1>

                <p>This website includes blog posts, projects of various kinds, and topics surrounding science, mainly
                    computer science, programming, and politics. It also acts as an outlet for my creativity, both in
                    the form of website design and writing. Writing is frustrating, it takes a lot of time, however once
                    the finished product is there, you can admire it, re-read and improve it; like a sculpture.</p>

                <blockquote>
                    I hate writing, I love having written.

                    <br><cite>Dorothy Parker</cite>
                </blockquote>

                <p>One key aspect of this website that I want to remember is that this is a place for my self
                    expression, not a CV for some future employer to look at. So much of our lives is absorbed entirely
                    by capitalism, drilled down to our core to a point where our very decision making is heavily
                    influenced by a motive for profit, or to be financially successful. Fully expressing ourselves
                    without the influence of capitalism is a difficult task in our day and age, near impossible in fact,
                    but it's important to try to do so.</p>

                <blockquote>
                    The ideas of the ruling class are in every epoch the ruling ideas, i.e. the class which is the
                    ruling material force of society, is at the same time its ruling intellectual force. The class which
                    has the means of material production at its disposal, has control at the same time over the means of
                    mental production, so that thereby, generally speaking, the ideas of those who lack the means of
                    mental production are subject to it.
                    <br><cite>Karl Marx</cite>
                </blockquote>

                <p>This website is also free from any and all forms of tracking. Not even just for statistics. I'm not
                    making money off of this website, and I'm not strategising for ideas about how to make the site more
                    popular. If you like the site, you can come back and read articles, and you can share. That's it :)
                </p>

                <p>Check out the <a href="https://codeberg.org/penloy/penloycom">official repository</a> for this
                    website.</p>

                <p><a href="/html/blm.php">Support BLM here</a></p>
            </main>
        </div>
    </div>
</body>

</html>